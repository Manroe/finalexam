// Shorthand for $( document ).ready()
$(function() {
    // build a convenience function to render each article
    var element = document.getElementById("blogs");
    element.innerHTML = "";
    function render_article(article) {
        element.innerHTML += "<div class=\"blog-post\">" + 
                            "<img class=\"img-responsive\" src=\"" + article['urlToImage'] + "\" height=\"400\">" +
                            "<a href=\"" + article['url'] + "\">" + 
                           "<h2 class=\"blog-post-title text-dark\">" + article['title'] + "</h2>" +
                            "<h5 class=\"blog-post-info text-muted\">" + article['description'] + "</h5>" + 
                            "</a>" +
                            "<p class=\"blog-post-meta\"> Source: " +
                            "<a href=\"" + article['source']['name'] + "\">" + article['source']['name'] + "</a>" +
                            " published at " + article['publishedAt'] + "</p>" +
                            "</div>";
    }
    // ====================================================

    // ====================================================
    // Construct your search_url
    // ... YOUR CODE GOES HERE ...
    var search_url = 'https://newsapi.org/v2/everything?' +
      'q=' + "Apple" + '&' + 'language=en&' +
      'pageSize=10&' +
      'sortBy=popularity&' +
      'apiKey=c95ff2e9cb53435e8f413ef505111d78';

    // ====================================================

    // ====================================================
    // Perform the AJAX operation
    $("button").click(function () {
      element.innerHTML = "";
      var searchTerm = $('#searchTerm').val();
      var articleCount = $('#numArticles').val();
      var sort = $('#sortBy').val();
      if(articleCount < 1 || articleCount > 100) { articleCount = 10 }
      if(sort != 'popularity' && sort != 'relevancy' && sort != 'publishedAt') { sort = 'relevancy' }
      search_url =
        'https://newsapi.org/v2/everything?' +
        'q=' + searchTerm + '&' + 'language=en&' +
        'pageSize=' + articleCount + '& sortBy=' + 
        sort + '&' +
        'apiKey=cd081ac64b6044c6ba98bf6f807c7140';

      $.ajax({
        url: search_url,
        dataType: "json",
        success: function(data) {
        // Get the total number of articles
        var n = data["totalResults"];
  
        // Get the array of articles
        var articles = data["articles"];
  
        console.log("Total Articles Found:" + n);
  
        articles.forEach(function(article) {
          // Process all of the articles on this page
          render_article(article);
        });
      },
      error: function(e) {
        console.log("AJAX Error: " + e);
      }
    });
  });
    // ====================================================
});
  